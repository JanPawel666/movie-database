<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use App\Genere;

class MovieGenereController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function index(Movie $movie)
    {
        return view('movie-genere.index', compact('movie'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function create(Movie $movie)
    {
        $attached = $movie->generes->pluck('id')->all();
        $remaining = Genere::all()->except($attached);
        return view('movie-genere.create', ['generes' => $remaining, 'movie' => $movie]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @param  \App\Movie   $movie
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Movie $movie)
    {
        $genere = Genere::find($request->input('genere_id'));
        if (!$genere || $movie->belongsToGenere($genere)) {
            return redirect()->back();
        }
        $movie->generes()->attach($genere);
        return redirect()->route('movie-genere.index', compact('movie'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Movie   $movie
     * @param  \App\Genere  $genere
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie, Genere $genere)
    {
        abort_if(!$movie->belongsToGenere($genere), 404);
        return view('movie-genere.edit', compact('movie', 'genere'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movie   $movie
     * @param  \App\Genere  $genere
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie, Genere $genere)
    {
        if (! $movie->belongsToGenere($genere)) {
            abort(404);
        }
        $movie->generes()->detach($genere);
        return redirect()->route('movie-genere.index', compact('movie'));
    }
}
