<?php

namespace App\Http\Controllers;

use App\Movie;
use App\Http\Requests\StoreMovieRequest;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $q = $request->query('q');
	$order = $request->query('order');
        $perPage = 7;

	switch ($order) {
	case 'date_desc':
		$sortAttr = 'release_date';
		$sortDir = 'desc';
		break;
	case 'date_asc':
		$sortAttr = 'release_date';
		$sortDir = 'asc';
		break;
	default:
		$sortAttr = '';
		$sortDir = '';
	}


        $movies = Movie::browse($q, $sortAttr, $sortDir, $perPage);

        return view('movie.index', compact('movies', 'order', 'q'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('movie.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Requests\StoreMovieRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMovieRequest $request)
    {
        $new_movie = Movie::create($request->all());
        return redirect()->route('movie.show', ['movie' => $new_movie]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function show(Movie $movie)
    {
        return view('movie.show', compact('movie'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie)
    {
        return view('movie.edit', compact('movie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Requests\StoreMovieRequest  $request
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(StoreMovieRequest $request, Movie $movie)
    {
        $movie->update($request->all());
        return redirect()->route('movie.show', compact('movie'));
    }
}
