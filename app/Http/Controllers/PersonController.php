<?php

namespace App\Http\Controllers;

use App\Person;
use App\Country;
use App\Http\Requests\StorePersonRequest;
use Illuminate\Http\Request;

class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $people = Person::paginate(7);
        return view('person.index', compact('people'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('person.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePersonRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePersonRequest $request)
    {
        $person = Person::create($request->validated());

        $countryId = $request->input('birth_country');
        if ($country = Country::find($countryId)) {
            $person->country()->associate($country)->save();
        }

        return redirect()->route('person.show', compact('person'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function show(Person $person)
    {
        return view('person.show', compact('person'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function edit(Person $person)
    {
        return view('person.edit', compact('person'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StorePersonRequest  $request
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function update(StorePersonRequest $request, Person $person)
    {
        $person->update($request->validated());

        $countryId = $request->input('birth_country');
        if ($country = Country::find($countryId)) {
            $person->country()->associate($country);
        }
        else {
            $person->country()->dissociate();
        }
        $person->save();

        return redirect()->route('person.show', compact('person'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Person $person)
    {
        $request->session()->flash('info', "Osoba $person->name została usunięta z bazy.");
        $person->delete();
        return redirect()->route('person.index');
    }
}
