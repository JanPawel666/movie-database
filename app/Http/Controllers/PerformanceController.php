<?php

namespace App\Http\Controllers;

use App\Performance;
use App\Movie;
use App\Person;
use Illuminate\Http\Request;

class PerformanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function selectActor(Movie $movie)
    {
        return view('performance.create.select_actor', compact('movie'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Movie   $movie
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function create(Movie $movie, Person $person)
    {
        return view('performance.create.create', compact('movie', 'person'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movie   $movie
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Movie $movie, Person $person)
    {
        $validated = $request->validate(
            ['character' => 'required|max:50'],
            ['character' => 'Postać']
        );
        $performance = new Performance;
        $performance->character = $validated['character'];
        $performance->movie()->associate($movie);
        $performance->actor()->associate($person);
        $performance->save();
        $request->session()->flash('info', 'Dodano nową rolę do filmu.');
        return redirect()->route('movie.show', compact('movie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Performance  $performance
     * @return \Illuminate\Http\Response
     */
    public function edit(Performance $performance)
    {
        return view('performance.edit', compact('performance'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Performance  $performance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Performance $performance)
    {
        $movie = $performance->movie;
        $performance->delete();
        return redirect()->route('movie.show', compact('movie'));
    }
}
