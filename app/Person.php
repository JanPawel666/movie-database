<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @property  \Illuminate\Database\Eloquent\Collection  $performances
 */
class Person extends Model
{
    /**
    * Indicates if the model should be timestamped.
    *
    * @var bool
    */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *php 
     * @var array
     */
    protected $fillable = ['name', 'birth_date', 'death_date', 'birth_city', 'height', 'bio', 'birth_country'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'birth_date' => 'date',
        'death_date' => 'date',
    ];

    public function performances()
    {
         return $this->hasMany('App\Performance');
    }

    public function country()
    {
        return $this->belongsTo('\App\Country', 'birth_country');
    }

    public function age()
    {
        return Carbon::now()->diffInYears($this->birth_date);
    }

    public function isDead()
    {
        return $this->death_date ? true : false;
    }

    /**
     * Return the age of a person on the day of death.
     * 
     * @return int|null
     */
    public function ageAtDeath()
    {
        return $this->death_date->diffInYears($this->birth_date);
    }
}
