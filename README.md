# Problem z Konfiguracją SQLite

Plik `.env` przed rozwiązaniem problemu:

```
DB_CONNECTION=sqlite
DB_HOST=
DB_DATABASE=
DB_PORT=
DB_USERNAME=root
DB_PASSWORD=
```

W pliku `config/database.php`:

```php
'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'url' => env('DATABASE_URL'),
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ],
```

Problem:

```
movie-database$ php artisan migrate:fresh
Illuminate\Database\QueryException : SQLSTATE[HY000] [14] unable to open
database file (SQL: PRAGMA foreign_keys = ON;)
...
```

Problemem jest obecność wiersza `DB_DATABASE=` w pliku `.env`. Brak wartość
po `DB_DATABASE` nie oznacza, że do stałej nie jest przypisana żadna wartość,
tylko że przypisany jest pusty ciąg znaków. Żeby przypisana została domyślna
wartość `database_path('database.sqlite')`, trzeba usunąć ten wiersz z pliku.

Trzeba utworzyć 

Plik `.env` poprawnie skonfgurowany.

```

DB_CONNECTION=sqlite

```


# Jak aktywować rozszerzenie SQLite w PHP na Linuxie?

Jeżeli przy próbie wykonania polecenia `php artisan migrate` Laravel
rzuca wyjątek:

```
[Illuminate\Database\QueryException] could not find driver
[PDOException] could not find driver
```

onacza to, że nie skonfigurowałeś rozszrzenia `php-sqlite`.

Pobierz rozszrzenie `php-sqlite` z repozytorium dystrybucji systemu,
a potem w pliku konfiguracji php (`/etc/php/php.ini` na Arch Linux)
odkomentuj ten wiersz:

```
;extension=sqlite.so
```


# Do Zrobienia

- [x] Dodać akcję `MovieController@update`
- [x] Rozbudować szablon `movie.edit`. Pola formularza powinny zachowywać
      wartość między żądaniami (jeżeli formularz został odrzucony).
- [x] Dodać akcję `MovieGenere@index` uruchamianą żądaniem
      `GET /movie-genere/{movie}`
- [x] Dodać szablon `movie.genere.index`
- [x] Umożliwić przypisanie nowego gatunku do filmu. Akcja
      `MovieGenereController@store` uruchamiana żądaniem
      `POST /movie-genere/{movie}/{genere}`
  - [x] Dodać instrukcje sprawdzające, czy `genre_id`
        przekazane w żadaniu odnosi się do gatunku
        istniejącego w bazie danych.
  - [x] Dodać kod sprawdzający, czy gatunek jest już
        przypisany do edytowanego filmu. Możliwe, że
        użytkownik nie używał przeglądarki do wykonania
        żądania i sam wybrał `genere_id`.
- [ ] Umożliwić wprowadzenie nowej nazwy gatunku do bazy
      Stworzyć kontrler `GenereController` i akcję `store`
      uruchamiany przez żądanie `POST /genere` dostępną z formularza
      w widoku `movie-genere.index`
- [x] Wyświetlanie i edycja relacji Film-Kraj
  - [x] Wyświetlanie listy
  - [x] Formularz dołączania do listy
  - [x] Akcja dołączania
  - [x] Usuwanie z listy:
    - [x] Formularz z ostrzeżeniem
    - [x] Akcja kontrolera
- [x] Wyświetlanie i edycja zasobu Person
  - [x] Wyświetlanie listy wszystkich osób
  - [x] Podstrona konkrentej osoby
  - [x] Dodawanie nowej osoby do bazy
    - [x] Formularz
    - [x] Akcja
  - [x] Edycja istniejącej osoby
    - [x] Formularz
    - [x] Akcja kontrolera
  - [x] Usuwanie osoby z bazy
    - [x] Pod ścieżką "edit" dodać drugi formularz z ostrzeżeniem
          uruchamiający akcję "destroy"
    - [x] Akcja kontrolera
- [x] Zasób ~~Movie-Person~~ Performance
  - [x] Lista z obsadą na podstronie filmu  (~~grupować względem osoby~~)
  - [ ] Lista z rolami na podstronie aktora (posortowane malejąco wg daty)
  - [x] Dodawanie roli do filmu (dostęp tylko z podstrony filmu)
    - [x] Wyszukiwarka aktorów do dodania  `GET /movie-person/{movie}`
    - [x] Formularz (podaj nazwę postaci)  `GET /movie-person/{movie}/{person}`
    - [x] Akcja kontrolera
  - [ ] Edycja
    - [ ] Formularz  `GET /role/{role}` ~~(role = id w talbeli pośredniej)~~
    - [ ] Rozbudować listę aktorów w szablonie *movie.show* o linki
          *edytuj/usuń* przenoszące do `/role/{role}`
    - [ ] Akcja kontrolera
  - [ ] Usuwanie
    - [ ] Dodać drugi formularz do widoku *role.edit* uruchamiający akcję
          `Role@destroy`
    - [ ] Implementacja akcji `Role@destroy`
  - [ ] Usunięcie zasobu `Person` usuwa też wszystkie jego role
        (zaimplementować mechanizm *on delete cascade*)
- [ ] Implementacja wyszukiwarki filmów (po tytule, kraju, roku premiery)
- [ ] Możliwość definiowania nowego kraju produkcji filmu
- [ ] Możliwość definiowania nowego gatunku filmu
- [ ] Uniemożliwić wstrzykiwanie HTML-a w formularzach
- [ ] Przesyłanie plakatów filmów
- [ ] Przesyłanie zdjęć osób kina
- [ ] Zoptymalizować czas żądań wymagających wyświetlenia gatunków i krajów
      przypisanych do filmu (patrz:
      `\App\Movie::with['generes', 'countries'])`)
- [ ] Dodać indeksy do tabel. Przedstawić w sprawozdaniu wynik zapytania
      z klauzulą `EXPLAIN` i porównanie czasu wykonania optymalizowanych
      zapytań.
