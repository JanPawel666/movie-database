@extends('layout')

@section('content')
<div class="row justify-center">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                Przypisz gatunek
            </div>
            <div class="card-body">
                
                <form method="POST" action="{{ route('movie-genere.store', compact('movie')) }}" class="mb-0">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-4 text-md-right">Film</div>
                        <div class="col-md-6 text-md text-truncate">{{ $movie->title }}</div>
                    </div> {{-- .row --}}
                    <div class="form-group row">                        
                        <label for="genere_id" class="col-md-4 col-form-label text-md-right">Gatunek</label>
                        <div class="col-md-6">
                            <select name="genere_id" class="custom-select">
                                @foreach($generes as $g)
                                    <option value="{{ $g->id }}">{{ $g->name }}</option>
                                @endforeach
                            </select>
                        </div> {{-- .col-md-6 --}}
                    </div> {{-- .form-group.row --}}

                    <div class="form-group row">
                        <div class="col-md-3 offset-md-4">
                            <button type="submit" class="btn btn-primary">Dodaj</button>
                            <a class="btn btn-link" href="{{ route('movie-genere.index', compact('movie')) }}">Wróć</a>
                        </div> {{-- .col-md-8.offset-md-4 --}}
                    </div> {{-- .form-group.row --}}
                </form>
            </div> {{-- .card-body --}}
        </div> {{-- .card --}}
    </div> {{-- .col-12.col-lg-8 --}}
</div> {{-- .row --}}
@endsection
