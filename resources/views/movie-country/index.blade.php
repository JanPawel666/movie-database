@extends('layout')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                Kraj produkcji
            </div>
            <div class="card-body">
                {{-- <h5 class="card-title">{{ $movie->title }}</h5> --}}
                <p>
                    <span class="text-muted mr-3">tytuł:</span>{{ $movie->title }}
                </p>
                @empty($movie->countries->all())
                    <p class="text-center">
                        Do filmu nie przypisano jeszcze krajów.<br/>
                    </p>
                    <p class="text-center">
                        <a href="{{ route('movie-country.create', compact('movie')) }}" class="btn btn-primary">Dodaj</a>
                    </p>
                @else
                    <p>
                        <table class="table table-borderless table-sm table-hover">
                            @foreach($movie->countries as $country)
                                <tr>
                                    <td>{{ $country->name }}</td>
                                    <td class="text-right"><a href="{{ route('movie-country.edit', compact('movie', 'country')) }}" class="text-danger">Usuń</a></td>
                                </tr>
                            @endforeach
                        </table>
                    </p>
                    <p class="mb-2">
                        <a href="{{ route('movie-country.create', compact('movie')) }}" class="btn btn-primary">Dodaj</a>
                        <a class="btn btn-link" href="{{ route('movie.show', compact('movie')) }}">Wróć</a>
                    </p>
                    @endempty
            </div>
          </div>
    </div> {{-- .col-12.col-lg-8 --}}
</div> {{-- .row --}}
@endsection
