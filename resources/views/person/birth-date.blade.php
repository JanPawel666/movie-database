@php

if (isset($person->birth_date)) {
    echo \App\Helpers\localizeDate($person->birth_date);
    if (! $person->isDead()) {
        echo " (", \App\Helpers\localizeAge($person->age()), ")";
    }
}
else {
    echo "Brak informacji";
}

@endphp