@php
if (! $person->birth_date) {
    echo "Brak informacji";
}
else if ($person->isDead()) {
    echo "Odszedł w wieku ", $person->ageAtDeath(), " lat ("
        , $person->birth_date->year, "-", $person->death_date->year, ")";
}
else {
    echo \App\Helpers\localizeAge($person->age()), " (", $person->birth_date->year, ")";
}
@endphp
