@extends('layout')

@section('content')
<div class="row justify-center">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                Nowa Osoba
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('person.store') }}" class="mb-0">
                    @csrf

                    <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">Nazwisko*</label>
                        <div class="col-md-6">
                            <input id="name" type="text" name="name" value="{{ old('name') }}"
                                   class="form-control @error('name') is-invalid @enderror"  autofocus>
                            @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div> {{-- .col-md-6 --}}
                    </div> {{-- .form-group.row --}}

                    <div class="form-group row">
                        <label for="birth_date" class="col-md-4 col-form-label text-md-right">Data urodzenia</label>
                        <div class="col-md-6">
                            <input id="birth_date" type="text" class="form-control @error('birth_date') is-invalid @enderror"
                            name="birth_date" placeholder="YYYY-MM-DD" value="{{ old('birth_date') }}" >
                            @error('birth_date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div> {{-- .col-md-6 --}}
                    </div> {{-- .form-group.row --}}

                    <div class="form-group row">
                        <label for="death_date" class="col-md-4 col-form-label text-md-right">Data śmierci</label>
                        <div class="col-md-6">
                            <input id="death_date" type="text" class="form-control @error('death_date') is-invalid @enderror"
                            name="death_date" placeholder="YYYY-MM-DD" value="{{ old('death_date') }}" >
                            @error('death_date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div> {{-- .col-md-6 --}}
                    </div> {{-- .form-group.row --}}

                    <div class="form-group row">                        
                        <label for="birth_country" class="col-md-4 col-form-label text-md-right">Kraj pochodzenia</label>
                        <div class="col-md-6">
                            <select name="birth_country" class="custom-select @error('birth_country') is-invalid @enderror">
                                <option>brak</option>
                                @foreach(\App\Country::orderBy('name', 'ASC')->get() as $c)
                                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                                @endforeach
                            </select>
                            @error('birth_country')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div> {{-- .col-md-6 --}}
                    </div> {{-- .form-group.row --}}

                    <div class="form-group row">
                        <label for="birth_city" class="col-md-4 col-form-label text-md-right">Miejsce urodzenia</label>
                        <div class="col-md-6">
                            <input id="birth_city" type="text" name="birth_city" value="{{ old('birth_city') }}"
                                    class="form-control @error('birth_city') is-invalid @enderror"  >
                            @error('birth_city')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div> {{-- .col-md-6 --}}
                    </div> {{-- .form-group.row --}}
    
                    <div class="form-group row">
                        <label for="height" class="col-md-4 col-form-label text-md-right">Wzrost</label>
                        <div class="col-md-6">
                            <input id="height" type="text" name="height" value="{{ old('height') }}"
                                   class="form-control @error('height') is-invalid @enderror"
                                   placeholder="cm"  >
                            @error('height')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div> {{-- .col-md-6 --}}
                    </div> {{-- .form-group.row --}}

                    <div class="form-group row">
                        <label for="bio" class="col-md-4 col-form-label text-md-right">Notka biograficzna</label>
                        <div class="col-md-6">
                            <textarea class="form-control @error('bio') is-invalid @enderror"
                                      id="bio" name="bio" rows="4"
                                      placeholder="Maks. 2048 znaków">{{ old('bio') }}</textarea>
                            @error('bio')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div> {{-- .col-md-6 --}}
                    </div> {{-- .form-group.row --}}

                    <div class="form-group row">
                        <div class="col-6 col-md-3 offset-md-4">
                            <button type="submit" class="btn btn-primary">Dodaj</button>
                        </div> {{-- .col-md-8.offset-md-4 --}}
                        <div class="col-6 col-md-3 text-right">* Pole wymagane</div>
                    </div> {{-- .form-group.row --}}
                </form>
            </div> {{-- .card-body --}}
        </div> {{-- .card --}}
    </div> {{-- .col-12.col-lg-8 --}}
</div> {{-- .row --}}
@endsection
