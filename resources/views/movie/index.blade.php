@extends('layout')

@section('content')
<div class="row">
    <div class="col-12">
        <h1 >Filmy</h1>
    </div>
    <div class="col-12 col-md-8 mb-3 text-right text-md-left">
        <form method="GET" class="form-inline mb-0">
            <input name="q" type="text" value="{{ Request::query('q') }}" class="form-control mr-md-2 mb-3 mb-md-0">
            <select name="order" class="custom-select mr-md-2 mb-3 mb-md-0">
                <option value="relevance">trafność</option>
                <option value="date_desc" {{ $order == 'date_desc' ? 'selected' : '' }}>najpierw nowsze</option>
                <option value="date_asc" {{ $order == 'date_asc' ? 'selected' : '' }}>najpierw starsze</option>
            </select>
            <button type="submit" class="btn btn-primary">Szukaj</button>
        </form>
    </div>
    <div class="col-12 col-md-4 text-md-right mb-3">
        <a href="{{ route('movie.create') }}" class="btn btn-primary">Dodaj film</a>
    </div>
    <div class="col-12">
        @if($movies->isEmpty())
            Nic nie znaleziono.
        @else
            <div class="list-group">
                @foreach($movies as $m)
                    <a class="list-group-item list-group-item-action" href="{{ route('movie.show', ['movie' => $m]) }}">
                        <p class="font-weight-bolder">{{ $m->title }}</p>
                        <p><table>
                            <tr class="align-top">
                                <td class="text-muted">Premiera:</td>
                                <td class="pl-4">{{  \App\Helpers\localizeDate($m->release_date)  }}</td>
                            </tr>
                            <tr class="align-top">
                                <td class="text-muted">Gatunek:</td>
                                <td class="pl-4">@include('movie.generes', ['movie' => $m])</td>
                            </tr>
                            <tr class="align-top">
                                <td class="text-muted">Kraj:</td>
                                <td class="pl-4">@include('movie.countries', ['movie' => $m])</td>
                        </table></p>
                        <p class="mb-0">
                            @isset($m->summary)
                                {{ $m->summary }}
                            @else
                                <span class="font-italic">Brak opisu</span>
                            @endisset
                        </p>
                    </a>
                @endforeach
            </div>
        @endif
        </p>
    </div> <!-- .col-12.col-lg-8 -->
</div> <!-- .row -->
@if($movies)
    <div class="row">
        <div class="col-12">
            {{ $movies->appends(['q' => $q, 'order' => $order])->links() }}
        </div>
    </div>
@endif
@endsection
