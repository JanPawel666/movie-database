@extends('layout')

@section('content')
<div class="row mb-3">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                Dodawanie nowej roli do filmu
            </div>
            <div class="card-body">
                <p>
                    <table>
                        <tr>
                            <td class="text-muted">Tytuł:</td>
                            <td class="pl-3">{{ $movie->title }}</td>
                        </tr>
                    </table>
                </p>
                <p class="mb-0">Wybierz odtwórcę:</p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        @if(! $people = \App\Person::paginate(7))
            Baza osób jest pusta.
        @else
            <div class="list-group">
                @foreach($people as $person)
                    <a class="list-group-item list-group-item-action" href="{{ route('performance.create', compact('movie', 'person')) }}">
                        <p class="font-weight-bolder">{{ $person->name }}</p>
                        <p><table>
                            <tr class="align-top">
                                <td class="text-muted">Wiek:</td>
                                <td class="pl-4">@include('person.age-inline')</td>
                            </tr>
                            <tr class="align-top">
                                <td class="text-muted">Pochodzenie:</td>
                                <td class="pl-4">@include('person.birth-country')</td>
                            </tr>
                        </table></p>
                    </a>
                @endforeach
            </div>
        @endif
        </p>
    </div> <!-- .col-12.col-lg-8 -->
</div> <!-- .row -->
@if($people)
    <div class="row">
        <div class="col-12">
            {{ $people->links() }}
        </div>
    </div>
@endif
@endsection
