@if(Session::has('info'))
<div class="row">
    <div class="col-12">
        <div class="alert alert-primary" role="alert">
            {{ Session::get('info') }}
        </div>
    </div>
</div>
@endif
