<?php

return [
    'date' => 'Pole :attribute nie zawiera poprawnej daty.',
    'date_format' => 'Pole :attribute nie zawiera daty w formacie ":format".',
    'max' => [
        'string' => 'Pole :attribute nie może zawierać więcej niż :max znaków.',
    ],
    'required' => 'Pole :attribute jest wymagane.',

    'attributes' => [
        'character' => 'Postać',
    ],
];
