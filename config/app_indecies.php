<?php

return [
    'movie' => [
        'release_date' => [
            'asc' => env('MOVIE_RELEASE_DATE_ASC_INDEX_NAME'),
            'desc' => env('MOVIE_RELEASE_DATE_DESC_INDEX_NAME'),
        ],
    ],
];

